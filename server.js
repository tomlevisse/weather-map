/*server.js
An app to get weather information from OpenWeather Api
5 day forecast

Author : Tom Levisse
Creation date : 23/10/2020
Licence : Levisse
*/

//node modules required
var express = require('express'),
	bodyParser = require('body-parser'),
	request = require('request'),
	moment = require('moment'),
	session = require('express-session'),

	app = express();
//requiring routess
var indexRoutes = require('./routes/index');

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));//to allow access to public folder
app.use(bodyParser.urlencoded({ extended: true }));

// passport config
app.use(require("express-session")({
	secret: "This is Man Utd!",
	resave: false,
	saveUninitialized: false
  }));

app.use('/', indexRoutes);

app.listen(process.env.PORT || 8080,function(){
	console.log("The Weather App server has started!");
  });
