var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var request = require('request');
var moment = require('moment');

var app = express();


var apiKey = 'c399108a2e5b082bf0868e1d38670c1d';
var pais = 'fr';

router.get('/', function (req, res) {
	res.render('index', {
		weather: null,
		city: null,
		country: null,
		currTemp: null,
		tempLow: null,
		tempHigh: null,
		currWeather: null,
		currWind: null,
		currHumid: null,
		windShort: null,
		currPressure: null,
		currCloud: null,
		currCompass: null,
		currDegree: null,
		visibility: null,
		icon: null,
		code: null,
		time: null,
		sunrise: null,
		sunset: null,
		error: null
	});
})

router.post('/', function (req, res) {
	var city = req.body.city;//var from form in index.ejs
	var url = `http://api.openweathermap.org/data/2.5/forecast?q=${city}&units=metric&lang=${pais}&appid=${apiKey}`
	console.log(city);
	request(url, function (err, response, body) {
		if (err) {
			console.log('error:', err);
			res.render("index", { weather: null, error: 'Error, please try again' });
		} else {
			//console.log('body:', body);
			var weather = JSON.parse(body);//parsing API response

			console.log(weather.list[1]);//print current Weather
			var currWeather = [];


			currWeather['message'] = weather.message;

			if (currWeather['message'] != 'city not found') {
				if (weather.list[0] != undefined) {
					//if no errors in the API respnse
					//-------------------------------------
					var NbTimeStamp = weather.cnt;//var number of timeStamps
					console.log('Number of time Stamp:' + NbTimeStamp);
					for (var i = 0; i < NbTimeStamp; i++) {
						//var t = weather.list[i].main.temp


						//current time and date
						currWeather['currTime'] = weather.list[0].dt;
						//currWeather['currTime'] = weather.list[0].dt.txt;
						//Convert from UTC to GMT
						currWeather['currTime'] = moment.unix(currWeather['currTime']).format("DD-MM-YYYY HH:mm:ss");
						//sunrise
						currWeather['sunrise'] = weather.city.sunrise;
						//Convert from UTC to GMT
						currWeather['sunrise'] = moment.unix(currWeather['sunrise']).format("HH:mm:ss");
						//sunset
						currWeather['sunset'] = weather.city.sunset;
						//Convert from UTC to GMT
						currWeather['sunset'] = moment.unix(currWeather['sunset']).format("HH:mm:ss");
						//current temperature
						currWeather['currTemp'] = Math.round(weather.list[0].main.temp);
						// short text description (ie. rain, sunny, etc.)
						currWeather['description'] = weather.list[0].description;
						// today's high temp
						currWeather['highTemp'] = Math.round(weather.list[0].main.temp_max);
						// today's low temp
						currWeather['lowTemp'] = Math.round(weather.list[0].main.temp_min);
						// humidity (in percent)
						currWeather['humidity'] = Math.round(weather.list[0].main.humidity);
						//Visibility, meter
						currWeather['visibility'] = weather.list[0].visibility;
						// barometric pressure (converting hPa to inches)
						currWeather['pressure'] = weather.list[0].main.pressure;//* 0.02961339710085
						// barometric pressure (rounded to 2 decimals)
						currWeather['pressure'] = currWeather['pressure'].toFixed(2);
						// 50x50 pixel png icon
						currWeather['icon'] = "http://openweathermap.org/img/w/" + weather.list[0].weather.icon + ".png";
						//weather code
						currWeather['code'] = weather.list[0].weather.id;
						// cloud cover (in percent)
						currWeather['cloudiness'] = weather.list[0].clouds.all;
						// wind speed
						currWeather['windSpeed'] = Math.round(weather.list[0].wind.speed);
						// wind direction (in degrees)
						currWeather['windDegree'] = weather.list[0].wind.deg;
						// wind direction (compass value)
						currWeather['windCompass'] = Math.round((currWeather['windDegree'] - 11.25) / 22.5);

						// array of direction (compass) names
						var windNames = new Array("North", "North Northeast", "Northeast", "East Northeast", "East", "East Southeast",
							"Southeast", "South Southeast", "South", "South Southwest", "Southwest",
							"West Southwest", "West", "West Northwest", "Northwest", "North Northwest");
						// array of abbreviated (compass) names
						var windShortNames = new Array("N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW");
						// convert degrees and find wind direction name
						currWeather['windDirection'] = windNames[currWeather['windCompass']];

						// current temperature
						var response = "Current Weather: " + currWeather['currTemp'] + "\xB0 and " + currWeather['description'];
						if (currWeather['windSpeed'] > 0) {// if there's wind, add a wind description to the response
							response = response + " with winds out of the " + windShortNames[currWeather['windCompass']] + " at " + currWeather['windSpeed'] + " miles per hour";
						}


					}
					//current city
					currWeather['currCity'] = weather.city.name;
					//current country
					currWeather['currCountry'] = weather.city.country;

//------------------------ end of parsing info form API -----------------------------//
					//let message = `Il fait ${weather.list[0].main.temp} degrés à ${weather.city.name}!`;
					var message = `${weather.list[0].weather[0].description}`;
					console.log(message);
					//	console.log(currWeather['lowTemp']);
					//	console.log(currWeather['highTemp']);
					res.render('index', {
						weather: message,
						city: currWeather['currCity'],
						country: currWeather['currCountry'],
						currTemp: currWeather['currTemp'] + "\xB0",
						tempLow: currWeather['lowTemp'] + "\xB0",
						tempHigh: currWeather['highTemp'] + "\xB0",
						currWeather: currWeather['description'],
						currWind: currWeather['windSpeed'],
						currHumid: currWeather['humidity'],
						windShort: windShortNames[currWeather['windCompass']],
						currPressure: currWeather['pressure'],
						currCloud: currWeather['cloudiness'],
						currCompass: currWeather['windCompass'],
						currDegree: currWeather['windDegree'],
						visibility: currWeather['visibility'],
						icon: currWeather['icon'],
						time: currWeather['currTime'],
						code: currWeather['code'],
						sunrise: currWeather['sunrise'],
						sunset: currWeather['sunset'],
						error: null
					});
				}
				//si il y a une erreur quelconque
				else {
					res.render('index', { weather: null, error: 'Error, please try again' });
				}

			}
			else {
				res.render('index', { weather: null, error: 'Error, city not found ! ' });
			}





		}
	});
});
module.exports = router;
